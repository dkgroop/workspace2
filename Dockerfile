FROM php:7.2-fpm

# Переключаем Ubuntu в неинтерактивный режим — чтобы избежать лишних запросов
ENV DEBIAN_FRONTEND noninteractive 

RUN apt-get update && apt-get install  -y --allow-unauthenticated \
                locales \
                wget unzip vim \
        		libfreetype6-dev \
		        libjpeg62-turbo-dev \
        		libpng-dev \
                libgearman-dev \
                libmcrypt-dev \
                libxml2-dev \
                libfcgi0ldbl \
                uuid-dev

# Устанавливаем локаль
RUN echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
RUN locale-gen 

ENV LANG ru_RU.UTF-8  
ENV LANGUAGE ru_RU:ru  
ENV LC_ALL ru_RU.UTF-8

RUN  docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
	&& docker-php-ext-install -j$(nproc) exif \
	&& docker-php-ext-install -j$(nproc) gd \
	&& docker-php-ext-install -j$(nproc) intl \
	&& docker-php-ext-install -j$(nproc) dom \
	&& docker-php-ext-install -j$(nproc) soap \
	&& docker-php-ext-install -j$(nproc) pcntl \
	&& docker-php-ext-install -j$(nproc) mysqli \
	&& docker-php-ext-install -j$(nproc) pdo pdo_mysql \
	&& docker-php-ext-install -j$(nproc) zip

RUN pecl install redis-5.1.1

RUN pecl install xdebug-2.9.6

RUN cd /tmp \
    && wget https://github.com/wcgallego/pecl-gearman/archive/gearman-2.0.3.zip \
    && unzip gearman-2.0.3.zip \
    && mv pecl-gearman-gearman-2.0.3 pecl-gearman \
    && cd pecl-gearman \
    && phpize \
    && ./configure \
    && make -j$(nproc) \
    && make install \
    && cd / \
    && rm /tmp/gearman-2.0.3.zip \
    && rm -r /tmp/pecl-gearman

RUN pecl install uuid-1.0.4

RUN docker-php-ext-install opcache

RUN docker-php-ext-enable uuid redis xdebug gearman opcache

COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#RUN composer config -g github-oauth.github.com b4e212f72779450b1db53ccd1e5d19d1e96f9569
#RUN composer global require fxp/composer-asset-plugin  --no-interaction
#RUN composer global require hirak/prestissimo

RUN set -xe && echo "pm.status_path = /status" >> /usr/local/etc/php-fpm.d/zz-docker.conf

COPY php-fpm-healthcheck /usr/local/bin/php-fpm-healthcheck

WORKDIR /var/app

